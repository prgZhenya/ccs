// Fill out your copyright notice in the Description page of Project Settings.


#include "CCS_Character.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"

#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"

#include "Kismet/KismetMathLibrary.h"

// Sets default values
ACCS_Character::ACCS_Character()
{
	// Set this character to call Tick() every frame. 
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	CameraBoom->bEnableCameraLag = true;
	CameraBoom->CameraLagSpeed = 5;

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	GetCharacterMovement()->bOrientRotationToMovement = true;

	CameraBoom->TargetArmLength = 1000;
	CameraRotator = FRotator(-80.f, 0, 0);
	CameraBoom->SetRelativeRotation(CameraRotator);
}

// Called when the game starts or when spawned
void ACCS_Character::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACCS_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


//----	Inputs	----//	//----	Inputs	----//	//----	Inputs	----//	

// Called to bind functionality to input
void ACCS_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Enhanced Input Controller
	if (UEnhancedInputComponent* EIC = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EIC->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ACCS_Character::OnInputMove);
		EIC->BindAction(JumpAction, ETriggerEvent::Started, this, &ACCS_Character::OnInputStartJump);
		EIC->BindAction(JumpAction, ETriggerEvent::Canceled, this, &ACCS_Character::OnInputEndJump);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("'%s' Failed to find an Enhanced Input Component!"), *GetNameSafe(this));
	}
}


//----	Inputs	----//	//----	Inputs	----//	//----	Inputs	----//

void ACCS_Character::OnInputMove(const FInputActionValue& Value)
{
	FVector2D MovementVector = Value.Get<FVector2D>();

	float X = FMath::Abs(MovementVector.X);	
	float Y = FMath::Abs(MovementVector.Y);
	float Scale = (X > Y) ? X : Y;

	AddMovementInput(FVector(MovementVector, 0), Scale);
}

void ACCS_Character::OnInputStartJump(const FInputActionValue& Value)
{
	Jump();
}

void ACCS_Character::OnInputEndJump(const FInputActionValue& Value)
{
	StopJumping();
}



