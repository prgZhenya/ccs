// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CCS_PlayerController.generated.h"

class UInputMappingContext;
class UInputAction;

UCLASS()
class COMBOCOMBATSYSTEM_API ACCS_PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ACCS_PlayerController();

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

protected:

	virtual void SetupInputComponent() override;

	// To add mapping context
	virtual void BeginPlay();
	
};
